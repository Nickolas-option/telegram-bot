import sqlite3
from telegram_bot import config as cfg


class Planner:
	def __init__(self, db_name: str = cfg.DB_NAME) -> None:
		self.db_name = db_name
		self._create_table()

	def _create_table(self):
		with sqlite3.connect(self.db_name) as conn:
			query = '''
			CREATE TABLE IF NOT EXISTS planner(
				userid INT,
				plan TEXT,
			)
			'''
			conn.execute(query)
